<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('hola',function(){
    return "hola  mundo";
});
Route::get('/', function () {
    return view('auth.login');
});

    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');;
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('widgets', function () {
        return view('layouts.widgets');
    });
    Route::get('layout', function () {
        return view('layouts.layout');
    });
    Route::get('metrics', function () {
        return view('layouts.metrics');
    });
    
    Route::resource('products','ProductController'); 
    Route::resource('users','UsersController');
    Route::resource('users.post','PostController');
    
//----------- reset password -----
    

    
    // cre q es get showLinkRequestForm
    // Route::get('reset_password','Auth\AccountsController@reset_password');
    // Route::get('reset_password','Auth\AccountsController@forgot_password');

});
