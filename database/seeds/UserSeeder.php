<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name'=>'erwinlero',
            'lastname'=>'quinteros',
            'userrole'=>'STUDENT',
            'zipcode'=>'75457135',
            'aboutme'=>'HOLA COM ETAN YO SOY ERWIN QUINTEROS VILLARROEL',
            'licensenumber'=>'1234567',
            'email'=>'erwinler@gmail.com',
            'password'=>bcrypt('123456789')

        ]);
        User::create([
            'name'=>'MATIAS',
            'lastname'=>'ARAUJO',
            'userrole'=>'TRANSPORT',
            'zipcode'=>'100100',
            'aboutme'=>'LOREMLOREMLORELOREMLOREMLOREMLOREM',
            'licensenumber'=>'HMJ1234',
            'email'=>'admin@admin.com',
            'password'=>bcrypt('admin123')

        ]);
        
        factory(User::class, 5)->create();
    }
}

