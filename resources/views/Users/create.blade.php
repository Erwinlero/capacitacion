@extends('layouts.principal')
@section('content')

<form class="form-horizontal" action="{{ url('users')}}" method="post" enctype="multipart/form-data">
{{ csrf_field() }}

@include('users.partials.form',['Modo'=>'crear'])
</form> 
@include('layouts.scripts')
@stop