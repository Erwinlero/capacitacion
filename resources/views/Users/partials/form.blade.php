
<div class="row wrapper border-bottom white-bg">
    <div class="col-lg-12">
        <h2>Users</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong></strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Users Registration</h5>
                </div >
                <div class="ibox-content">
                    <div class="form-group row">
                        {!! Form::label('Name', 'Name* ' , ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-4">
                                <input type="text" name="name" id="name" placeholder="Name" class="form-control" required  
                                value="{{ isset($user->name)?$user->name:'' }}">
                            </div>
                    </div>
            
                    <div class="hr-line-dashed"></div>

                    <div class="form-group row">
                        {!! Form::label('Last Name', 'Last Name*', ['class' => 'col-sm-2 control-label'])!!}
                            <div class="col-sm-4">
                                <input type="text" name="lastname" id="lastname" placeholder="Last Name" class="form-control" required 
                                value="{{ isset($user->lastname)?$user->lastname:'' }}">      
                            </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group row">
                    {!! Form::label('Email', 'Email*', ['class' => 'col-sm-2 control-label'])!!}
                    <div class="col-sm-4">
                        <input id="email" type="email" name="Email" id="Email" placeholder="Email" class="form-control @error('Email') is-invalid @enderror" required=""
                        value="{{ isset($user->email)?$user->email:'' }}">
                        @error('Email')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                        @enderror 
                    </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group row">
                        {!! Form::label('Photo', 'Photo*', ['class' => 'col-sm-2 control-label'])!!}
                        @if(isset($user->photo))
                        <br>
                        <img src="{{ asset('storage').'/'.$user->photo }}" alt="" width="100">
                        
                        @endif
                        <input type="file" name="photo" id="photo" accept="image/x-png,image/gif,image/jpeg" value="{{ isset($user->photo)?$user->photo:'' }}">
                        
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group row">
                        {!! Form::label('Password', 'Password*', ['class' => 'col-sm-2 control-label'])!!}
                        <div class="col-sm-4">
                            <input id="password" type="password" name="password" id="password" placeholder="Password" class="form-control" required=""
                            value="{{ isset($user->password)?$user->password:'' }}">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="hr-line-dashed"></div>

                    <div class="form-group row">
                        {!!Form::label('About me', 'About me*', ['class' => 'col-sm-2 control-label'])!!}
                        <div class="col-sm-4">
                        <textarea type="text" name="aboutme" id="aboutme" placeholder="About me" class="form-control" required="" cols="30" rows="3">{{ isset($user->aboutme)?$user->aboutme:'' }}</textarea>   
                        </div>
                    </div>
                    

                    <div class="hr-line-dashed"></div>

                    <div class="form-group row">
                    {!! Form::label('Zip Code', 'Zip Code*' , ['class' => 'col-sm-2 control-label'])!!}
                        <div class="col-sm-4">
                            <input type="text" name="zipcode" id="zipcode" placeholder="Zip Code" class="form-control" required=""
                            value="{{ isset($user->zipcode)?$user->zipcode:'' }}">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                        
                    <div class="form-group row">
                    {!! Form::label('License Number', 'License Number*', ['class' =>  'col-sm-2 control-label'])!!}
                        <div class="col-sm-4">
                            <input type="text" name="licensenumber" id="licensenumber" placeholder="License Number" class="form-control" required=""
                            value="{{ isset($user->licensenumber)?$user->licensenumber:'' }}">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group row">
                    {!! Form::label('User Role', 'User Role*' , ['class' => 'col-sm-2 control-label'])!!}
                        <div class="col-sm-4">
                            <input type="text" name="userrole" id="userrole" placeholder="User Role" class="form-control" required=""
                            value="{{ isset($user->userrole)?$user->userrole:'' }}">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-4 col-md-7">
                                <input type="submit" class="btn btn-primary" value="{{ $Modo=='create' ? 'Agregar':'Save' }}"> 
                                    <a href="{{ url('users') }}" class="btn btn-default">Cancel</a>  
                            </div>
                        </div>
                    </div>       
                </div>                  
            </div>  
        </div>         
     </div>  
</div>


    
    