@extends('layouts.principal')
@section('content')

@if(Session::has('Mensaje')){{
    Session::get('Mensaje')
}}
@endif
<div class="row wrapper border-bottom white-bg">
    <div class="col-lg-12">
        <h2>Users</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('layout') }}">Home</a>
            </li>
            <li class="active">
                <strong>Users</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper wrapper-contentt">
    <div class="row">
        <div class="class="col-lg-12>
            <div class="ibox-content">
                <a href="{{ url('/users/create') }}" class="btn btn-primary m-b">Add Users</a>
            
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead class="thead-light">
                        <tr>
                            <th>ID</th>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Lastname</th>
                            <th>Email</th>
                            <th>User Role</th>
                            <th width="200px">Acctions</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>
                            <img src="{{ asset('storage').'/'.$user->photo }}" class="img-circle" alt="" width="50">
                            </td>
                            <td>{{ $user->name}}</td>
                            <td>{{ $user->lastname}}</td>
                            <td>{{ $user->email}}</td>
                            <td>{{ $user->userrole}}</td>
                            <td>                           
                               <a class="btn btn-primary btn-xs" href="{{ route('users.show',$user->id) }}">Show</a> /

                                <a class="btn btn-success  btn-xs" href="{{ route('users.post.index',$user->id) }}">View Post</a> /

                                <a class="btn btn-warning btn-xs" href="{{ url('/users'.$user->id.'/edit') }}" enctype="multipart/form-data">Edit</a> /

                                <form method="post" action="{{ url('users/'.$user->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('¿Delete?');">Delete</button> 
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>
@endsection