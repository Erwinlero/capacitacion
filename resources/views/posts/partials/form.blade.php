<div class="row wrapper border-bottom white-bg">
    <div class="col-lg-12">
        <h2>Posts</h2>
        <ol class="breadcrumb">
            
            <li class="active">
                <strong></strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List Posts</h5>
                </div >
                <div class="ibox-content">

                

                    <div class="hr-line-dashed"></div>
                
                    <div class="form-group row">
                    {!! Form::label('tittle', 'Tittle*',['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-4">
                                <input type="text" name="case" id="case" placeholder="case" class="form-control" required 
                                value="{{ isset($post->case)?$post->case:'' }}">
                            </div>
                    </div>
            
                    <div class="hr-line-dashed"></div>

                    <div class="form-group row">
                    {!! Form::label('description', 'Description*',['class' => 'col-sm-2 control-label'])!!}
                        <div class="col-sm-4">
                           <textarea type="text" name="description" id="description" placeholder="description" class="form-control" required="" cols="30" rows="3" >{{ isset($post->description)?$post->description:'' }}</textarea>   
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-4 col-md-7">
                                <input type="submit" class="btn btn-primary" value="{{ $Modo=='create' ? 'Agregar':'Save' }}"> 
                                   <!-- aqui va lo q tengo en m bloc de ntoas-->
                                   <a href="{{ url('/users/'.$user.'/post') }}" class="btn btn-default">Cancel</a>
                            </div>
                        </div>
                    </div>       
                </div>                  
            </div>  
        </div>         
     </div>  
</div>


    
    