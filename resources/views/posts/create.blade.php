@extends('layouts.principal')
@section('content')
@include('layouts.head')
<form class="form-horizontal" action="{{ url('/users/'.$user->id.'/post')}}" method="post" enctype="multipart/form-data">
 
@csrf
@include('posts.partials.form',['Modo'=>'crear'])
</form> 
@include('layouts.scripts')
@stop