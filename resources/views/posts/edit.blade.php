@extends('layouts.principal')
@section('content')

<form action="{{ url('users/'.$user.'/post/'.$post->id) }}" method="post" class="form-horizontal" enctype="multipart/form-data"> 
{{ csrf_field() }}
{{ method_field('PATCH') }}

@include('posts.partials.form',['Modo'=>'editar'])
</form>

@stop