@extends('layouts.principal')
@section('content')

@if(Session::has('Mensaje')){{
    Session::get('Mensaje')
}}
@endif
<div class="row wrapper border-bottom white-bg">
    <div class="col-lg-12">
        <h2>Posts</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('layout') }}">Home</a>
            </li>
            <li class="active">
                <strong>Posts</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper wrapper-contentt">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-content">
            
            
            <a href="{{ route('users.post.create',$user->id) }}" class="btn btn-primary m-b">New Post</a>

                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead class="thead-light">
                        <tr>
                            <th>ID</th>
                            <th>Tittle</th>
                            <th>Description</th>
                            <th width="200px">Acctions</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            
                            <td>{{ $post->case}}</td>
                            <td>{{ $post->description}}</td>
                            <td>                           
                            <!--<a class="btn btn-info" href="{{ url('/users/'.@$user->id.'/post/',$post->id) }}">Show</a>-->

                                <a class="btn btn-primary" href="{{ url('/users/'.@$user->id.'/post/'.$post->id.'/edit') }}" enctype="multipart/form-data">Edit</a>

                                <form method="post" action="{{ url('/users/'.@$user->id.'/post/'.$post->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('¿Delete?');">Delete</button> 
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection