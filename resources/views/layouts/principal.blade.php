<!DOCTYPE html>
<html>
            <!-- head -->
            @include('layouts.head')
<body>

<div id="wrapper">

            <!-- navigation -->
            @include('layouts.cms.navigation')

    <div id="page-wrapper" class="gray-bg dashbard-1">
            
            <!-- topnavbar -->
            @include('layouts.cms.topnavbar')

            <!-- main menu -->
            @yield('content')
   
             <!-- footer-->
            @include('layouts.footer')
        
    
    </div>
</div>
            <!-- scripts -->
            @include('layouts.scripts')
</body>
</html>
