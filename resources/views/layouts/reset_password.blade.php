@extends('layouts.principal')
@section('content')
    <div class="button-toogle back">
        <a href="/">
            <img src="{{ asset('frontend/img/back.png') }}" alt="">
        </a>
    </div>
    <!-- ========================= SECTION CONTENT ========================= -->
    <section id="login">

        <div class="content-login mt-5">

            <div class="section-tilte text-center mb-5">
                <h1> <img src="{{ asset('frontend/img/logo-big.png') }}" alt="Concred"></h1>
            </div>


            <div class="content-form box-content-shadow">
                <div class="messages"></div>
                <form id="forgot-password" action="{{ url('reset_password') }}" onsubmit="site.forgotPassword(event)">
                    {{ csrf_field() }}
                    <div class="form-group mb-4">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" placeholder="Ingrese su email" required>
                    </div>

                    <div class="box-buttons-in">
                        <input type="submit" class="btn btn-primary w-100 mb-1" value="Recuperar Password">
{{--                        <a href="{{ url('/') }}" class="btn btn-primary w-100 mb-1">Recuperar Password</a>--}}
                    </div>
                </form>
            </div>

        </div><!-- content-form .//  -->
    </section>

    <!-- ========================= SECTION CONTENT END// ========================= -->
@endsection
