@extends('layouts.app')

@section('content')

<div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">ER+</h1>
            </div>
            <h3>Welcome to IN+</h3>
            <p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p>Login in. To see it in action.</p>
            <form method="POST" action="{{ route('login') }}" class="m-t" role="form" action="index.html">
                @csrf
                <div class="form-group">
                    <input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Username" required="">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="password" name="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password" required="">
                    
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                   
                @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}"><small>Forgot password?</small></a>
                @endif
                
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                
                @if (Route::has('register'))
                <a class="btn btn-sm btn-white btn-block" href="{{ route('register') }}">Create an account</a>
                @endif

            </form>
            <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
        </div>
    </div>

@endsection
