<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UpdateGenericClass;


class Post extends Model
{
    
    use UpdateGenericClass;

    protected $table='post';

    protected $fillable = [
        'case', 'description','user_creator_id',
    ];
    

    public function user(){
        return $this->belongsTo('App\Users', 'user_creator_id', 'id');
    }
    public function comment(){
        return $this->belongsToMany(Comment::class ,'comment','id', 'post_id')->withTimestamps();
    }

    public function getCaseAttribute($value)
    {
        return strtoupper($value);
    }




    //--------------SCOPES---------------//

    public function scopeByUser($query,$id){
        return $query->where('user_creator_id','=',$id);
    }
    public function scopeFindIqualId($query,$id){
        return $query->where('user_creator_id',$id);
    }


    
}



