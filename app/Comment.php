<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table='comment';
    public $timestamps = true;

    protected $fillable = [
        'tittle', 
        'description',
         'user_id', 
         'post_id',
    ];
    
    public function post(){
        return $this->belongsToMany(Post::class);
    }

    public function users(){
        return $this->belongsToMany('\App\Users','comment','id', 'user_id')->withTimestamps(); 
    }

    public function posts_comment(){
        return $this->belongsToMany('\App\Post','comment','id', 'post_id')->withTimestamps(); 
    }
}

