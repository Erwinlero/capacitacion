<?php

namespace App;


class Helper {
public static function add_img($request){

    $file = $request->file('photo');
    $name_file =time().$request->photo->getClientOriginalName();
    
    $file->move(public_path('/storage'),$name_file);

    return $name_file;
    }
    function delete_img($photo){
        unlink(public_path('storage/'.$photo)); 
    }

}
