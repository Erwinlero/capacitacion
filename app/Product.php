<?php
  
namespace App;
  
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use App\Traits\UpdateGenericClass;
   
class Product extends Model

{

    use UpdateGenericClass;
    protected $fillable = [
        'name', 'detail'
    ];
}