<?php
  
namespace App;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Traits\UpdateGenericClass;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Notifications\ResetPassword;
   
class Users extends Model
{
    use Notifiable;

    //
    use UpdateGenericClass;

    protected $table='users';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
    // relacion de 1-M
    public function post(){
        return $this->hasMany(Post::class, 'user_creator_id', 'id');
    }
    // relacion de N-M
    public function comments(){
        return $this->belongsToMany('\App\Comment','comment','id', 'user_id')->withTimestamps(); 
    }


    public function getNameAttribute($value)
    {
        return $this->attributes['name']=strtoupper($value);
    }
    public function getLastnameAttribute($value)
    {
        return strtoupper($value);
    }
    public function getUserroleAttribute($value)
    {
        return ucfirst($value);
    }
    

    //---------------SCOPES------------------//

    public function scopeFindId($query,$id){
        return $query->where('id','=',$id);

    }

}