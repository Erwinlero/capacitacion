<?php
  
namespace App\Http\Controllers;
  
use App\Product;
use Illuminate\Http\Request;
  
class ProductController extends Controller
{
//-------------------index-----------------------//

    public function index()
    {
        $products = Product::latest()->paginate(5);
  
        return view('products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

//-------------------create-----------------------//

    public function create()
    {
        return view('products.create');
    }
//-------------------store-----------------------//

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
  
        Product::create($request->all());
   
        return redirect()->route('products.index')
                        ->with('success','Product created successfully.');
                    
    }
//-------------------show-----------------------//

    public function show(Product $product)
    {

        return view('products.show',compact('product'));
    }
//-------------------edit-----------------------//
   
    public function edit(Product $product)
    {
        return view('products.edit',compact('product'));
    }
//-------------------update-----------------------//
  
    public function update(Request $request, Product $product, $id)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
  
        // $product->update($request->all());
        Product::updateData($id)->update($request);
  
        return redirect()->route('products.index')
                        ->with('success','Product updated successfully');
    }
//-------------------destroy----------------------//
    
    public function destroy(Product $product)
    {
        $product->delete();
  
        return redirect()->route('products.index')->with('success','Product deleted successfully');
    }
}