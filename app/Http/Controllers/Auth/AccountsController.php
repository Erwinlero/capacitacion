<?php

namespace App\Http\Controllers\Auth;

use App\PasswordReset;
use App\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Auth\generatePassword;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;

class AccountsController extends Controller
{

    public function reset_password(Request $request)
    {
        return view('layouts.reset_password');
    }
    public function validatePasswordRequest(Request $request){
        $user=Users::findEmail($request->email)->first();
       
        //verifiqued if the email exist
        if ($user === null) {
            return redirect()->back()->with(['email' => $request->email]);
        }
        //insert token in table password_reset
        PasswordReset::insertData($request->email,Str::random(60),Carbon::now());
       
        //get data of token 
        $token_data = PasswordReset::findEmail($request->email)->first();
        //send email
        if ($this->sendResetEmail($request->email, $token_data->token)) {
            return redirect()->back()->with('status','A reset link has been sent to your email address.');
        } else {
            return redirect()->back()->with('error','A Network Error occurred. Please try again.');
        }
    }

    private function sendResetEmail($email, $token){
        
        $user = users::findEmail($email)->select('username', 'email')->first();
        //link a send
        $link = Config::get('app.url') . ':8000/' . 'password/reset/' . $token . '?email=' . urlencode($user->email);
        try {
            $details=[           
                'to' =>  $link,
                'subject' => 'login por token',
                'mensaje' => $link
            ];
        Mail::to($email)->send(new \App\Mail\MailSend($details));
                
        return true;
        
            } catch (\Exception $e) {
                return false;
            }
    }

    public function resetPassword(Request $request){
        $request->validate(
        [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|confirmed',
            'token' => 'required' 
        ]);
        
        // Validate the token
        $tokenData = PasswordReset::findToken($request->token)->first();
        //PasswordReset::where('token',$request->token)->first();
        if ($tokenData === null) {
            return view('auth.password-reset'); 
        }

        // Redirect the user back if the email is invalid
        $user = users::findEmail($tokenData->email)->first();
        if ($user===null) return redirect()->back()->withErrors(['email' => 'Email not found']);

        //Hash and update the new password
        $password = $request->password;
        $user->password = Hash::make($password);
        $user->update(); 

        //login the user immediately they change password successfully
        Auth::login($user);

        //Delete the token
        PasswordReset::findEmail($user->email)->delete();

        $user=auth()->user()->id;
        return view('home')->with('user',$user);
    }    

    public function forgot_password(Request $request){
        $responseCode = 200;

        $request->validate([
            'email' => 'required'
        ]);
        $user= Users::getUserWithEmail($request->email)->first();

        if(isset($user)){
            $password= generatePassword(10);
            $user->password = bcrypt($password);
            Mail::raw('Su nueva contraseña es: '.$password, function ($message) use ($user){
                $message->subject('CONCRED: Nueva Contraseña')->to($user->email);
            });
            $user->save();
            $result = array(
                'detail' => 'Se ha enviado un email a su correo con la nueva contraseña.',
                'code' => 'success'
            );
        }else{
            $result = array(
                'code' => 'danger',
                'detail' => "El usuario no existe"
            );
            $responseCode = 409;
        }
        return response()->json($result, $responseCode);
    }
    

}
