<?php

namespace App\Http\Controllers;


use App\Users;
use App\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    
    public function index()
    {
        $users=Users::get();
        return view('users.index',compact('users'));
    }

    public function create()
    {

        return view('users.create');
        
    }

    public function store(Request $request)
    {

        $datosUser=request()->except('_token');
        
        if($request->hasFile('photo')){
        
            $datosUser['photo']=Helper::add_img($request);
            
        }
        Users::insert($datosUser);

        return redirect('users')->with('Success','Product created successfully.');
        
    }


    public function show(Users $user)
    {
            
            return view('users.show',compact('user'));
    }
    
    public function edit($id)
    {
        
        $user=Users::findOrFail($id);   
        return view('users.edit', compact('user'));

    }

    public function update(Request $request, $id)
    {
        //
        $datosUser=request()->except(['_token', '_method']);

        if($request->hasFile('photo')){

            $user= Users::findOrFail($id);

            Storage::delete('public/.$user->photo');

            $datosUser['photo']=$request->file('photo')->store('uploads','public');
        }
        // where('id','=',$id)->update($datosUser); va despues de users::
        Users::where('id','=',$id)->update($datosUser);
        return redirect('users')->with('Mensaje','user successfully modified');
    }

    public function destroy($id)
    {
        
        $user= Users::find($id);
            //dd($user);
                if(Storage::delete('public/.$user->photo')){
                    
                };
                $user->delete();
            return redirect('users');
    }
    

}
