<?php

namespace App\Http\Controllers;

use App\Post;
use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{

    public function index($id){

        $user = Users::find($id);

        $posts=Post::findIqualId($id)->get();
        return view('posts.index',compact('user'),compact('posts'));
         
    }

    public function create($id){
        $user = Users::find($id);
        return view('posts.create', compact('user'));
        
    }


    public function store(Request $request, $id){
        $datosUser=request()->except('_token');
        $datosUser['user_creator_id']=$id;
        Post::insert($datosUser);

        return redirect('users/'.$id.'/post');
    }

    
    public function edit(Request $request){

        $post=Post::find($request->post);
        $user=$request->user;
       
        return view('posts.edit', compact('post','user'));

    }

    
    
    public function update(Request $request , $id){

        $datosUser=request()->except(['_token', '_method']);
        ///$datosUser['user_creator_id']=$request->user;
        $post=Post::where('id','=',$request->post)->update($datosUser);
        return redirect('users/'.$request->user.'/post');

    }

    public function destroy(Users $user , Post  $post){
        
        $post->delete();
        return redirect('users/'.$user->id.'/post');

    }

    public function listaPost($id){
        $posts=Post::byUser($id)->get();
        $users=Users::paginate(5);

        return view('posts.index',compact('users','posts'));       
    }

}
